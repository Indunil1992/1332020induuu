module.exports = function() {
    this.clusters = [];

    this.clusters["fdf"] = {
        host: process.env.EndPoint_redisFdf,
        port: 6379,
        clusterModeEnabled: true
    };
};